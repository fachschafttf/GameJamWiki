# Ideen für die Zukunft

* Mal am LD Compo teilnehmen und einfach pro Kopf ein Team machen. Man kann sich dann ja trotzdem bisschen helfen  
* Zeitplan zu beginn der GJ aufstellen mit Milestones die jeweils erreicht werden sollen.  
* Mehr genre flexibilität, unsere letzten paar spiele waren vom Kern recht ähnlich, Könnten z.B. mal in richtung Strategie, systems game, oder komplett koop/multiplayer spiel machen  
* Man könnte versuchen von Anfang an ein spielbares Spiel zu haben und iterativ den *Spaß* zu verbessern. Hierfür muss man eventuell ein kleines Team sein, da man nicht im vorraus viel planen kann und sich das Spiel erst in den häufigen gametest meetings entwickelt
* WICHTIG: keine Kernfeatures die nicht am Samstag fertig sind! Stichwort: Multiplayer!!!!!
* Lautstärke slider im game
