 ## GameJame Wiki der Fachschaft TF

 Die [Fachschaft der Technischen Fakultät Freiburg](https://fachschaft.tf) hat ein kleines Team an Leuten die gerne an GameJams teilnehmen. Dies soll ein bisschen dokumentieren was man beachten muss und was bisher geschah.
 
 Dieses Repo lässt sich entspannt hier lesen:
 https://fachschafttf.gitlab.io/GameJamWiki 

Wiki kann hier bearbeitet werden:
https://gitlab.com/fachschafttf/GameJamWiki
