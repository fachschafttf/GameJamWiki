# Summary

* [Startseite](README.md) -> Diese Seite
* [Ideen](Ideen.md) &rarr; Was kann man in Zukunft (anders?) machen?
* [Für die nächste GameJam](Dokumentation/FuerDieNaechsteGameJam.md)

## Dokumentation

* [Ludum Dare](Dokumentation/LudumDare.md)
* [GMTK](Dokumentation/GMTK.md)
* [GenerellCool](Dokumentation/GenerellCool.md)
* [Frameworks](Dokumentation/Frameworks.md)
* [Coole Projekte](Dokumentation/CooleProjekte.md)
* [Unity](Dokumentation/Unity.md)
  * [Setup](Dokumentation/UnityOnLinux.md) 
  * [How To Unity](Dokumentation/HowToUnity.md)
  * [Unity and Git](Dokumentation/UnityAndGit.md)
  * [Testing (in Unity)](Dokumentation/TestingInUnity.md)

