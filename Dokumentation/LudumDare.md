# Ludum Dare

Die Game Jam [Ludum Dare](https://ldjam.com/) findet immer ungefähr im April und Oktober statt.

## Regeln

* Vor der Jam wird über ein Theme abgestimmt (z.B. Delay the Inevitable, Unstable)
  * Das Thema ist oft nur das Setting und man kann eigentlich machen was man will
* 72h (Sa 2AM bis Di 2AM)
* Wenn man nach der Jam viele ander Spiele bewertet wird das eigene Spiel sichtbarer für andere Gruppen.

## Sollte man insbesondere drauf Achten

* [cooles Thumbnail mit WebGl Logo, wie z.B. bei der LD49.](https://gitlab.com/fachschafttf/ludumdare49/-/blob/main/src/Thumbnail/BlockItThumb.png)
  * so sehen andere direkt dass das man nichts downloaden/installieren muss

## Teilnahmen

* [LD44](https://gitlab.com/NoeF/ld44) Theme: Your life is currency
* [LD46](https://gitlab.com/fachschafttf/ld46) Theme: Keep it alive
* [LD47](https://gitlab.com/fachschafttf/ludumdare47) Theme: Stuck in a loop
* [LD48](https://gitlab.com/fachschafttf/ludumdare48) Theme: Deeper and deeper
* [LD49](https://gitlab.com/fachschafttf/ludumdare49) Theme: Unstable
* [LD50](https://gitlab.com/fachschafttf/ludumdare50) Theme: Delay the inevitable
