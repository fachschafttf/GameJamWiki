# Packages die sinnvoll sind zum Benutzen:

- Input System: Besseres Input System, etwas komplizierter zum Verstehen, dafür aber viel generalisierter in der Benutzung.

- Universal RP: Sehr wichtig, für WebGL Build und generelles Funktionieren von vielen Features

- Shader Graph: Visuelles Programmieren von Shadern

- Cinemachine: Tolle Kamerafahrten.
