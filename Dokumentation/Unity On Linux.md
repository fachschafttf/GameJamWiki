# Setup Guide for Linux  
A small guide for anyone that wants to use Unity3D on their Linux (tested on Ubuntu) machine.  
As of April 2022, working on Linux was unproblematic.

## Unity Hub
Unity regularly releases new Editor Versions.  
The Unity Hub is organizing the different versions and can be used to add new *Modules* (for example WebGL Build Support) to the individual installations  

#### Installation  
Should be possible as described [here](https://docs.unity3d.com/hub/manual/InstallHub.html#install-hub-linux) 

#### Sign In / Create Account
  
to be continued ....
