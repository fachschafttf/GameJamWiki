# Brainstorming
## Ideen Level 
- Gamemechanik
- Story
- Setting
Sollte nicht verworfen werden nur weil einer der Kategorien festgelegt wurde

## Brainstorming
Erstes Board sollte leer sein für komplett freie Ideen Samlung

Neue Brainstorming Variante Entwickeln!!!! 
- In Vorbesprechung schon ausarbeiten

Ideen:  
- zuerst jeder für sich Brainstormen (ersten 30 min eigene Mindmap)
  anschließendes zusammenkommen für eine gemeinsame Idee
- es müssen sich nicht alle an der Spielsuche beteilit sein  (sollte für alle Passen)  
  Es muss jeder hinter der Idee stehen sonst geht der Spaß schnell verloren  
- Nur Core gameplay loop sollte am Anfang stehen, der rest kann sich noch organisch entwickeln

# Wahl des templates
Ob 2D oder 3D kann immer noch geändert werden -> muss nicht vorher fest gelegt werden
Grafikwahl wird vom team getroffen aber kann von den Grafikern eingeschränkt werden 

# Spiel an Tag 1
Das grund Spiel muss am ersten Tag stehen.

# Projekt bauen
Ist in der pipeline immer recht langsam -> mal auf lokalem PC bauen

# XXX (NoeF) als Projektmanager
Der will kein Projektmanager sein, bzw. nicht so rüber kommen. Macht das aber immer gut.
Wäre cool wenn das vorher definiert wird.

## Scrum Master
Scrum Master als Rolle (wenn jemand bock darauf hat).  
Ziel gerichtetes Arbeiten sollte von anfang an mehr fokusiert werden.  

## Prefab orientiertes Arbeiten
Nicht an Scene Arbeiten sondern an Prefabs diese von Anfang an in der Main scene haben.  
Integration passiert von Anfang an.  
Viel motivierender wenn man direkt die änderungen im Spiel sieht.  
Testingtool mit Pipeline bringt uns hier permanente Test möglichkeit.  

### Workflow
Neuen am Anfang erklären wie der Workflow mit Unity(oder anderer Gameengine) und Git funktoniert  

# Credits
Gib den Leuten eine Rolle (Spitznamen)

# Aufgabenverteilung
Ist wichtig

# Gamedesign
Macht jemand und priorisiert was gemacht wird. Überlget sich features, levels, featurekonzeption, und features, hat den Überblick wie die features zusammen passen.

## Koordination zwischen den Teams muss besser werden
Es sollte allen klar sein wie die Evolution des Spiels ist.  
Klare Strucktur was von den jeweiligen Teams (Art, Design, Coding, ...) als Nächstes und im Generellen noch benötigt wird.

# Spezialisierung
Jeder wählt seine main klasse also viele Futzis die ihr Ding machen.
Man sagt was man so priomäßig machen will. Dadurch gibts immer ne Ansprechperson.

# Discord
Braucht struktur. Sag den neuen Leuten, dass alles darüber läuft.

# Projekt struktur
Besprechen was welcher Ordner macht

# Tests beim Programmieren sind cool ;) 
Last mal rausfinden wie man tests verwendet das man funktionen auch direkt testen kann (ohne Scenario aufzubauen)  
Pipeline kann Tests durchführen  
Man kann auch irgendwie Tests in Unity integrieren  

# Ignore everything above
keine Kreativität einschänken :P
