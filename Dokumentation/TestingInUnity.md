# Testing in Unity
What is testing, why do you test and how do you test your code in Unity?  
Here we mainly want to focus on the third question.

Also take a look in the [example project](https://gitlab.com/fachschafttf/unity-examples/unity-test-framework)

## Why Test?
- Verify Code works (duhh!)
  - you know when your code works!
- working with multiple devs on semi related parts of the project
  - you know that you broke other code by fixing your bug (ping-ponging bugs back and forth between jammers)


## Test Driven Development (TDD)
The basic idea is to write your tests before you implement the functions.

This could be helpful for the jam:  
- This forces you to think about the structure of your code before you waste time implementing, and you can spot mistakes before you have dont them.
- One person that is good at code architecture can quickly describe what the code should do by just writing down the expected functionality.
- The remaining task can be split among the team easily.

With a bigger team / project, these points get even more important!

## Manual Testing
You can build small scenes and test all scripts you create directly in the unity editor. It might be hard to model the project in a way to allow modular manual testing, and you might still depent on other peoples work.
Manual Testing is usually fine for small GameJams, but with growing team size, project size or complexity, one might prefer a better system.

## TODO
- we also skip stuff *end to end test* and *integration test*
- unit test
- https://docs.unity3d.com/Packages/com.unity.test-framework@1.1/manual/index.html
