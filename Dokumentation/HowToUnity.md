# Unity3D
Unity3D is a Game Engine.  

## Installation
* Install the Unity Hub
* Install the correct Unity Editor (with all Modules).
  * you probably want the Editor Version that the group uses for the project
  * you probably dont need to install any *additional* modules (if the group builds the project over a pipeline, you dont need to build the game locally)  

More Information here in the [Unity Manual](https://docs.unity3d.com/hub/manual/index.html)  

## Unity together with Git


## First Steps in Unity
The [Unity Documentation](https://docs.unity3d.com/Manual/index.html) is also an amazing ressource!  

### The Interface
This is what you will working with, so you probably should learn how to use it sooner than later  
Some *important* stuff:  
* Scene View  (to be continued)

## Ressources to keep learning
* [Brackeys YouTube channel](https://www.youtube.com/c/Brackeys) is sadly discontinued, but there are over 450 AWESOME videos.  

### Shader

* [World bending effect](https://www.youtube.com/watch?v=SOK3Ias5Nk0)
* [CatlikeCoding](https://catlikecoding.com/unity/tutorials/) very mathematic approach
* [Shaderslab](http://www.shaderslab.com/shaders.html) collection of cool effects
* [Unity talk 2015](https://www.youtube.com/watch?v=epixwRw80MM) did not actually watch it but usually high quality 
* [Unity talk 2016](https://www.youtube.com/watch?v=3penhrrKCYg) did not actually watch it but usually high quality 
* [Unity docs](https://docs.unity3d.com/Manual/Shaders.html)
