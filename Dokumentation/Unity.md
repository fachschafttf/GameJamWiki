# Unity stuff

### WebGL performance settings
[WebGL performance considerations](https://docs.unity3d.com/Manual/webgl-performance.html)

### Unity multiplayer
Wurde überarbeitet
[Unity Multiplayer Networking](https://docs-multiplayer.unity3d.com/)

### Testing in Unity
[Testing in Unity](Dokumentation/TestingInUnity.md)
