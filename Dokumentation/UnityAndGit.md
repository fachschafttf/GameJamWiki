# Best Practice - New Unity Project and GIT 

This guide could also be named __This worked for me - New Unity Project and Git__.  
There is not *the best way* to manage a Unity project in git. 

This small guide does not try to provide you with more (*accurate*, *correct* and *up-to date*) information than the official [Unity Documentation](https://docs.unity3d.com/Manual/index.html). It should rather give a small overview and point to important topics, it also serves as a quick starting point if you are completely new to one or multiple of these topics and want to collaborate in a team. 


## Version Control
A version control system helps us to collaborate and share the project on different machines. It can also be a save guard to rollback previous changes.

Not using version control is probably *not* an option.


### git
You probably already know git (which will be our version control of choice), if not you could get started [here](https://git-scm.com/docs/gittutorial). 

It has a steep learning curve but as long as everything is well maintained, a small collection of commands will suffice:

```
git status  
git pull  
git add  
git commit  
git push  
```

If you do not like working with the Terminal, you can also find a vast collection of graphical tools (GUI) to help you out. Many IDEs also have git supported.

Your Ubuntu machine probably already has git installed, on windows you can use [Git for Windows](https://gitforwindows.org/)


### Large File Storage git-lfs

[git-lfs](https://git-lfs.github.com/) is an open source Git extension for versioning large files. There are many different large files in a unity project like audio samples, graphics, ...  
LFS allows smarter storage of those files and helps at keeping the repository at a manageable size, and helps keeping cloning/fetching times short without changing the git workflow.

You WILL get unexpected results if LFS is not set up properly.


### Installing git-lfs

EVERY USER needs to set up git lfs for their user account once by running

```
git lfs install
```

### configuring git-lfs

We need to tell the project which files should get the special treatment.  
We can do this by either running a command like this to track all files of that type:
```
git lfs track "*.wav"
```

or by simply editing the ```.gitattributes``` file directly.  
Make sure that this file is tracked by git, for example by running: 

```
git add .gitattributes
git commit -m "adding .gitattributes file to configure LFS"
```

Your *.gitattributes* could look like [this](https://gist.github.com/nemotoo/b8a1c3a0f1225bb9231979f389fd4f3f)

### .gitignore

There will be some files that are in the project folder, but should not be under version control. (for example personal setting).  
The ```.gitignore``` file specifies which files should be completely ignored by git.  
Setting up this file on the first and maintaining it whenever you encounter a file that should not be tracked is a good idea and will help to keep the repository clean.  

It is not only possible to have multiple of those files (one per directory), but I would also recommend to have a second *gitignore* in the unity project folder. [more details](https://stackoverflow.com/questions/3305869/are-multiple-gitignores-frowned-on)

Be careful on how your *gitignore* is set up. If your repository looks different on a different machine, there might be local files that are ignored on git, hence never got in the version control.

You can find a good general *.gitignore* for the unity project folder [here](https://github.com/github/gitignore/blob/main/Unity.gitignore)

## Unity workflow

Unity3D is a game engine with a component based design.  
Everything you place in the world is a [*GameObject*](https://docs.unity3d.com/ScriptReference/GameObject.html).  
You can tell a *GameObject* how to behave by adding one or multiple [components](https://docs.unity3d.com/ScriptReference/Component.html).  
A component might be predefined, for example the [*Rigidbody*](https://docs.unity3d.com/ScriptReference/Rigidbody.html) that will tell the physics loop to affect this *GameObject* with stuff like gravity.  
For more unique behaviour you can create your own [*MonoBehaviour*](https://docs.unity3d.com/Manual/class-MonoBehaviour.html) in C#

Lets be realistic here: there is no way I can *shortly* explain everything. But here is a short list of stuff that is popular for questions and mistakes by new Unity Developers. (As perceived by my subjective eye)

* .meta files
* Prefabs
* Merging of Scenes and Prefabs
* before you commit


### meta files

Unity frequently checks the content of the Assets folder to create and store additional data in meta files (more details below). If you move/rename/delete the files in the [Unity Project window](https://docs.unity3d.com/Manual/ProjectView.html), everything will be handled for you.  
This is why i would recommend to **not change files directly in the file browser**.

Unity creates [meta](https://docs.unity3d.com/Manual/AssetMetadata.html) files for [asset](https://docs.unity3d.com/Manual/AssetWorkflow.html) files and folders. These are hidden in the *Project window* and might also be [hidden](https://en.wikipedia.org/wiki/Hidden_file_and_hidden_directory) in your file system by default.  

These *meta* files contain important information about how the asset is used in the project. (the meta file of a material asset for example stores which game objects use that material). It is important that the *meta* files and the original asset stay together.

**So please add and commit your meta files to the version control**

### Prefabs

Unity’s [Prefab](https://docs.unity3d.com/Manual/Prefabs.html) system allows you to create, configure, and store a [GameObject](https://docs.unity3d.com/Manual/class-GameObject.html)
complete with all its components, property values, and child GameObjects as a reusable Asset. The Prefab Asset acts as a template from which you can create new Prefab instances in the [Scene](https://docs.unity3d.com/Manual/CreatingScenes.html).  

### Merging of Scenes and Prefabs
If more than one member changed the same scene or prefab at the same time a merge conflict will occur.    
You will be told that there are different changes and you can either choose a version or manually choose the exact changes you want, many files can be merged automatically (if the changes are in different lines of a C## script for example).  
Prefabs and scenes can not be automatically merged by default. I have not yet found time to set up a system that allows the automatic merging of these files ([it appears that it is rather easy](https://docs.unity3d.com/Manual/TextSceneFormat.html)). So please communicate with your team for now and try to not work on the same scene / prefab at the same time to keep merge conflicts minimal.

### before you commit (common mistakes)

* make sure you saved your scene
* double check if unity processed all assets (closing unity can help to ensure everything is saved.)

## Example Folder Structure

This is a possible example of a folder structure that works in general.  
*src* (source) and *ci* (continuous integration) are not unusual to see.  
Make sure that the unity Project is not the root folder of the git project´

```
project(git root folder)
│   README.md
│   .gitignore    
│   .gitattributes    
│
└───src
│   │
│   └───Models
│   │
│   └───Music
│   │
│   └───unity project root folder
│       │   .gitignore
│       │
│       └───Assets
│       │
│       └───ProjectSettings
│   
└───Builds
│     │  ...   
│     │
│     └─ ...
│   
└───ci  
.   │  ...   
.   │
.   └─ ...       
```
