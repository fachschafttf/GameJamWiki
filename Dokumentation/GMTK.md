# GMTK

Die Game Maker's Toolkit GameJam findet jährlich statt, meist im Sommer:
 * August [2019](https://itch.io/jam/gmtk-2019)
 * Juli [2020](https://itch.io/jam/gmtk-2020)
 * Juni [2021](https://itch.io/jam/gmtk-2021) 
 
Das Thema wird meistens mit einem *super Hype* Video auf Youtube bekannt gegeben!  
Das Ziel ist es natürlich am Ende in dem [Video der besten Spiele](https://www.youtube.com/playlist?list=PLc38fcMFcV_vnAZjugCRdKr8_8d_y_rRl) dabei zu sein!

## Regeln

Anders als die LD, geht es hier mehr um Spiel*DESIGN*.  
Die genauen Regeln gibt es auf der jeweiligen Jam Seite

## Teilnahmen

* [GMTK19](https://gitlab.com/fachschafttf/gmtk19gamejam) Theme: Only one
* [GMTK20](https://gitlab.com/NoeF/gmtk2020)
